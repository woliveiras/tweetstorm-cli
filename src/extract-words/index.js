function extractWords(text) {
    const extractedTextWithoutSpaces = text.replace(/\r?\n|\r/g, '')
    const extractedTextByEnd = extractedTextWithoutSpaces.split(' ')
    const extractedTextLimit = extractedTextByEnd.length
    let store = []
    let storeLength = null
    let cursor = ''
    const characterLimit = 80

    for (const word of extractedTextByEnd) {
        if (cursor.length <= characterLimit) {
            cursor = `${cursor} ${word}`
        } else {
            store.push(cursor.trim())
            storeLength = store.length
            cursor = ''
        }
    }

    return store
}

module.exports = extractWords