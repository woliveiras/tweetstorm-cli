function tweetStormGenerator(tweetStore, tweetStoreLength) {
    let tweets = []
    let tweetCounter = 0

    for (const item of tweetStore) {
        tweetCounter++
        tweets.push(`${tweetCounter}/${tweetStoreLength} ${item}`)
    }

    return tweets
}

module.exports = tweetStormGenerator