## Tweetstorm CLI

> Esta é uma Tweetstorm CLI para gerar tweets enumerados!

## Como executar

Será necessário possuir instalado o Node.js (versão >= 8) ou o NVM.

Caso não possua o Node.js instalado, de preferência para o NVM seguindo este tutorial [gerenciando versões do Node.js](https://woliveiras.com.br/posts/utilizando-versoes-antigas-do-nodejs/), se estiver utilizando o Windows, siga [esta documentação](https://github.com/coreybutler/nvm-windows) e se estiver utilizando MacOS, siga a [documentação oficial](https://github.com/creationix/nvm).

Com o NVM instalado em sua máquina, execute o comando `nvm use` na raiz deste repositório e o ambiente estará pronto para execução.

## Rodando a CLI

Temos um arquivo `input.txt` na raiz do projeto, é ali onde colocamos o texto a ser "quebrado" pela CLI e transformado em um tweetstorm.

Basta copiar e colar seu texto para o `input.txt` e executar o comando `node index` e a CLI irá apresentar o resultado na tela.

![Mágia!](https://i.giphy.com/media/12NUbkX6p4xOO4/giphy.webp)