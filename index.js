const fs = require('fs')
const tweetStormGenerator = require('./src/tweet-storm-generator')
const extractWords = require('./src/extract-words')

const inputFile = fs.readFileSync('input.txt', 'utf-8')

const textsStore = extractWords(inputFile)
const tweets = tweetStormGenerator(textsStore, textsStore.length)

console.log('Tweet STORM!');
console.log(tweets)